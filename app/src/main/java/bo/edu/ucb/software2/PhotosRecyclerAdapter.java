package bo.edu.ucb.software2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Leonardo on 30/11/2015.
 */
public class PhotosRecyclerAdapter extends RecyclerView.Adapter<PhotosRecyclerAdapter.PhotoHolder> {

    private Context context;
    private LayoutInflater inflater;
    private final File imgFilesDirectory;
    private List<String> pathFiles;
    private RecyclerView recyclerView;

    public PhotosRecyclerAdapter(Context context, File imgFilesDirectory) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        if (!imgFilesDirectory.isDirectory())
            throw new IllegalArgumentException("imgFilesDirectory is not a directory.");
        this.imgFilesDirectory = imgFilesDirectory;
        String[] pathFiles = this.imgFilesDirectory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith("jpg");
            }
        });
        this.pathFiles = new ArrayList<>(Arrays.asList(pathFiles));
        Collections.sort(this.pathFiles);
//        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//            @Override
//            public void onChanged() {
//                Log.i(getClass().getName(), "data observer actually getting called");
//                pathFiles = PhotosRecyclerAdapter.this.imgFilesDirectory.list(new FilenameFilter() {
//                    @Override
//                    public boolean accept(File dir, String filename) {
//                        return filename.endsWith("jpg");
//                    }
//                });
//            }
//        });
    }



    @Override
    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_view_layout, parent, false);
        PhotoHolder photoHolder = new PhotoHolder(view);
//        int minWidth = this.recyclerView.getWidth() / 3;
//        int minHeight = (int) ((minWidth * 9) / 16f);
//        photoHolder.cardView.setMinimumWidth(minWidth);
//        photoHolder.cardView.setMinimumHeight(minHeight);
//        Log.i(getClass().getName(), "onCreateViewHolder w: " + minWidth + ", h: " + minHeight);
//        Log.i(getClass().getName(), "onCreateViewHolder cvw: " + photoHolder.cardView.getWidth() + ", cvh: " + photoHolder.cardView.getHeight());
        return photoHolder;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(PhotoHolder holder, final int position) {
        int targetWidth = this.recyclerView.getWidth() / 3;
        int targetHeight = (int) ((targetWidth * 9) / 16f);
//        int targetWidth = holder.cardView.getWidth();
//        int targetHeight = holder.cardView.getHeight();
//        int targetWidth = holder.getImgIcon().getWidth();
//        int targetHeight = holder.getImgIcon().getHeight();
//        Log.i(getClass().getName(), "onBindViewHolder w: " + targetWidth + ", h: " + targetHeight);
//        int targetWidth = this.recyclerView.getWidth() / 3;
//        int targetHeight = this.recyclerView.getHeight() / 3;
        new ImageLoadAsyncTask(holder).execute(targetWidth, targetHeight, position);
    }

    @Override
    public void onViewRecycled(PhotoHolder holder) {
        super.onViewRecycled(holder);
        holder.getImgPhoto().setImageDrawable(context.getResources().getDrawable(R.drawable.ic_image_black_48dp));
    }

    @Override
    public int getItemCount() {
        return this.pathFiles.size();
    }

    public void addPath(String path) {
        this.pathFiles.add(path);
        notifyItemInserted(this.pathFiles.size() - 1);
    }

    public File getPhotoFileAt(int position) {
        if (position >= 0 && position < this.pathFiles.size()) {
            return new File(this.imgFilesDirectory, pathFiles.get(position));
        } else return null;
    }

    public void removeFile(int position) {
        this.pathFiles.remove(position);
        notifyItemRemoved(position);
    }

    public class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imgPhoto;
        private CardView cardView;

        public PhotoHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.imgPhoto = (ImageView) itemView.findViewById(R.id.imgPhoto);
            this.cardView = (CardView) itemView.findViewById(R.id.cardView);
        }

        public ImageView getImgPhoto() {
            return imgPhoto;
        }

        public void setImgPhoto(Bitmap bitmap) {
            this.imgPhoto.setImageBitmap(bitmap);
        }

        @Override
        public void onClick(View v) {
            Snackbar.make(v, "Photo selected: " + getAdapterPosition(), Snackbar.LENGTH_SHORT).show();
        }
    }

    public class ImageLoadAsyncTask extends AsyncTask<Integer, Void, Bitmap> {

        private PhotoHolder holder;

        public ImageLoadAsyncTask(PhotoHolder holder) {
            this.holder = holder;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            int targetW = params[0];
            int targetH = params[1];
            int position = params[2];
            if (holder.getAdapterPosition() != position)
                cancel(true);

            String absolutePath = new File(imgFilesDirectory, pathFiles.get(position)).getAbsolutePath();
//            Log.i(getClass().getName(), "Path to be loaded as Bitmap " + absolutePath);
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(absolutePath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(targetW == 0 ? 0 : photoW / targetW, targetH == 0 ? 0 : photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, bmOptions);
            if (holder.getAdapterPosition() != position)
                cancel(true);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            holder.setImgPhoto(bitmap);
        }
    }

}
