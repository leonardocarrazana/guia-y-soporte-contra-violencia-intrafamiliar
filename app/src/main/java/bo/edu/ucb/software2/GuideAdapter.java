package bo.edu.ucb.software2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Leonardo on 01/12/2015.
 */
public class GuideAdapter extends RecyclerView.Adapter<GuideAdapter.GuideHolder> {

    private String[] texts;
    private LayoutInflater inflater;

    public GuideAdapter(Context context, String[] texts) {
        this.texts = texts;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public GuideHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_row_layout, parent, false);
        return new GuideHolder(view);
    }

    @Override
    public void onBindViewHolder(GuideHolder holder, int position) {
        holder.txvTitle.setText(texts[position]);
    }

    @Override
    public int getItemCount() {
        return this.texts.length;
    }

    public class GuideHolder extends RecyclerView.ViewHolder {

        TextView txvTitle;

        public GuideHolder(View itemView) {
            super(itemView);
            txvTitle = (TextView) itemView.findViewById(R.id.txvTitle);
        }
    }

}
