package bo.edu.ucb.software2;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnReportActionListener} interface
 * to handle interaction events.
 */
public class ReportsFragment extends Fragment {

    private OnReportActionListener mListener;

    private EditText etxFullName;
    private EditText etxEmail;
    private EditText etxPhoneNumber;
    private EditText etxDescription;

    public ReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports, container, false);
        this.etxFullName = (EditText) view.findViewById(R.id.etxFullName);
        this.etxEmail = (EditText) view.findViewById(R.id.etxEmail);
        this.etxPhoneNumber = (EditText) view.findViewById(R.id.etxPhoneNumber);
        this.etxDescription = (EditText) view.findViewById(R.id.etxReport);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.report_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSend:
                String fullName = this.etxFullName.getText().toString();
                String email = this.etxEmail.getText().toString();
                String phoneNumber = this.etxPhoneNumber.getText().toString();
                String description = this.etxDescription.getText().toString();
                Report report = new Report(fullName, email, phoneNumber, description);
                onSendPressed(report);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSendPressed(Report report) {
        if (mListener != null) {
            mListener.onSendReport(report);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnReportActionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnReportActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnReportActionListener {
        void onSendReport(Report report);
    }

    public class Report {

        private String fullName;

        private String email;

        private String phoneNumber;

        private String description;

        public Report(String fullName, String email, String phoneNumber, String description) {
            this.fullName = fullName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.description = description;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return "Report{" +
                    "fullName='" + fullName + '\'' +
                    ", email='" + email + '\'' +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", description='" + description + '\'' +
                    '}';
        }
    }

}
