package bo.edu.ucb.software2;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuideFragment extends Fragment implements RecyclerView.OnItemTouchListener {

    private RecyclerView recyclerView;
    private ClickListener listener;
    private GestureDetector gestureDetector;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (ClickListener) context;
        } catch (ClassCastException e) {
            Log.e(getClass().getName(), "Activity should implement ClickListener");
            throw e;
        }
    }

    public GuideFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.gestureDetector = new GestureDetector(getContext(), new MyGestureListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guide, container, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewGuide);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        String[] titles = getContext().getResources().getStringArray(R.array.titles);
        this.recyclerView.setAdapter(new GuideAdapter(getContext(), titles));
        this.recyclerView.addOnItemTouchListener(this);
//        this.recyclerView.addOnItemTouchListener((RecyclerView.OnItemTouchListener) getActivity());
        return view;
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        this.gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                int position = recyclerView.getChildAdapterPosition(view);
                listener.onClick(position);
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }

    public interface ClickListener {

        void onClick(int position);

//        void onLongClick(int position);
    }
}
