package bo.edu.ucb.software2;

/**
 * Created by Leonardo on 02/12/2015.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Created by Leonardo on 30/11/2015.
 */
public class VideosRecyclerAdapter extends RecyclerView.Adapter<VideosRecyclerAdapter.VideoHolder> {

    private Context context;
    private LayoutInflater inflater;
    private final File imgFilesDirectory;
    private List<String> pathFiles;
    private RecyclerView recyclerView;

    public VideosRecyclerAdapter(Context context, File imgFilesDirectory) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        if (!imgFilesDirectory.isDirectory())
            throw new IllegalArgumentException("imgFilesDirectory is not a directory.");
        this.imgFilesDirectory = imgFilesDirectory;
        String[] pathFiles = this.imgFilesDirectory.list();
        this.pathFiles = new ArrayList<>(Arrays.asList(pathFiles));
        Collections.sort(this.pathFiles);
    }



    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_view_layout, parent, false);
        VideoHolder videoHolder = new VideoHolder(view);
        videoHolder.getImgPhoto().setImageDrawable(context.getResources().getDrawable(R.drawable.ic_movie_black_48dp));
        return videoHolder;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, final int position) {
//        Log.i(getClass().getName(), "onBindViewHolder w: " + targetWidth + ", h: " + targetHeight);
        new ThumbnailLoadAsyncTask(holder).execute(position);
    }

    @Override
    public void onViewRecycled(VideoHolder holder) {
        super.onViewRecycled(holder);
        holder.getImgPhoto().setImageDrawable(context.getResources().getDrawable(R.drawable.ic_image_black_48dp));
    }

    @Override
    public int getItemCount() {
        return this.pathFiles.size();
    }

    public void addFile(String fileName) {
        this.pathFiles.add(fileName);
        notifyItemInserted(this.pathFiles.size() - 1);
    }

    public void removeFile(String fileName) {
        int position = Collections.binarySearch(this.pathFiles, fileName);
        removeFile(position);
    }

    public void removeFile(int position) {
        this.pathFiles.remove(position);
        this.notifyItemRemoved(position);
    }

    public File getVideoFileAt(int position) {
        if (position >= 0 && position < this.pathFiles.size()) {
            return new File(this.imgFilesDirectory, pathFiles.get(position));
        } else return null;
    }

    public class VideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imgPhoto;

        public VideoHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.imgPhoto = (ImageView) itemView.findViewById(R.id.imgPhoto);
        }

        public ImageView getImgPhoto() {
            return imgPhoto;
        }

        public void setImgPhoto(Bitmap bitmap) {
            this.imgPhoto.setImageBitmap(bitmap);
        }

        @Override
        public void onClick(View v) {
            Snackbar.make(v, "Video selected: " + getAdapterPosition(), Snackbar.LENGTH_SHORT).show();
        }
    }

    public class ThumbnailLoadAsyncTask extends AsyncTask<Integer, Void, Bitmap> {

        private VideoHolder holder;

        public ThumbnailLoadAsyncTask(VideoHolder holder) {
            this.holder = holder;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            int position = params[0];
            String absolutePath = new File(imgFilesDirectory, pathFiles.get(position)).getAbsolutePath();
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(absolutePath, MediaStore.Video.Thumbnails.MINI_KIND);
            if (holder.getAdapterPosition() != position)
                this.cancel(true);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            holder.setImgPhoto(bitmap);
        }
    }

}

