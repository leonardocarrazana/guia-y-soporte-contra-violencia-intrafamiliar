package bo.edu.ucb.software2;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Leonardo on 02/12/2015.
 */
public class AudioPlayer {

    private MediaPlayer player;

    public void prepareForPlay(String audioFile) throws IOException {
        this.player = new MediaPlayer();
        this.player.setDataSource(audioFile);
        this.player.prepare();
    }

    public void startPlaying() {
        if (this.player == null)
            throw new IllegalStateException("Player not prepared for playing");
        this.player.start();
        this.player.setScreenOnWhilePlaying(true);
    }

    public void pausePlaying() {
        if (this.player == null)
            throw new IllegalStateException("Player not prepared for playing");
        this.player.pause();
    }

    public void stopPlaying() {
        if (this.player == null)
            throw new IllegalStateException("Player not prepared for playing");
        this.player.stop();
        this.player.release();
        this.player = null;
    }

    public void seekTo(int millisec) {
        if (this.player != null)
            this.player.seekTo(millisec);
    }

}
