package bo.edu.ucb.software2;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class AudiosFragment extends Fragment implements RecyclerView.OnItemTouchListener {


    private RecyclerView recyclerView;
    private AudiosRecyclerAdapter adapter;
    private GestureDetector gestureDetector;

    public AudiosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adapter = new AudiosRecyclerAdapter(getContext(), getContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC));
        this.gestureDetector = new GestureDetector(getContext(), new MyGestureListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_items, container, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.recyclerView.setAdapter(adapter);
        this.recyclerView.addOnItemTouchListener(this);
        return view;
    }

    public void updateContent(String fileName) {
//        this.adapter.notifyDataSetChanged();
        this.adapter.addFile(fileName);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        this.gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener implements DeleteDialogFragment.OnDeleteActionListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                int position = recyclerView.getChildAdapterPosition(view);
                File file = adapter.getAudioFileAt(position);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "audio/aac");
                startActivity(intent);
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                int position = recyclerView.getChildAdapterPosition(view);
                File file = adapter.getAudioFileAt(position);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DeleteDialogFragment dialog = DeleteDialogFragment.newInstance(file.getAbsolutePath(), position);
                dialog.setmListener(this);
                dialog.show(fm, "DeleteDialogFragment");
                super.onLongPress(e);
            }
            super.onLongPress(e);
        }

        @Override
        public void onDelete(String fileToDelete, int position) {
            adapter.removeFile(position);
        }
    }


}
