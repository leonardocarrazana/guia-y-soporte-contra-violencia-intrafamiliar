package bo.edu.ucb.software2;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.io.File;

public class DeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String FILE_TO_DELETE_KEY = "file_to_delete_key";
    public static final String FILE_POSITION_DELETE_KEY = "file_position_delete_key";

    private OnDeleteActionListener mListener;

    public DeleteDialogFragment() {
    }

    public static DeleteDialogFragment newInstance(String filePath, int position) {
        DeleteDialogFragment fragment = new DeleteDialogFragment();
        Bundle args = new Bundle();
        args.putString(FILE_TO_DELETE_KEY, filePath);
        args.putInt(FILE_POSITION_DELETE_KEY, position);
        fragment.setArguments(args);
        return fragment;
    }

    public void setmListener(OnDeleteActionListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        AlertDialog dialog = builder.setTitle(R.string.delete_question_title)
                .setMessage("Delete file: " + getArguments().getString(FILE_TO_DELETE_KEY, "N/A") + "?")
                .setPositiveButton(R.string.delete_button, this)
                .setNegativeButton(R.string.cancel_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();
        return dialog;
    }

    public void onDeletePressed(String fileToDelete, int position) {
        if (mListener != null) {
            mListener.onDelete(fileToDelete, position);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            mListener = (OnDeleteActionListener) context;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(context.toString()
//                    + " must implement OnDeleteActionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        File file = new File(getArguments().getString(FILE_TO_DELETE_KEY));
        file.delete();
        onDeletePressed(getArguments().getString(FILE_TO_DELETE_KEY), getArguments().getInt(FILE_POSITION_DELETE_KEY));
    }

    public interface OnDeleteActionListener {
         void onDelete(String fileToDelete, int position);
    }

}
