package bo.edu.ucb.software2;

import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

public class PhotosFragment extends Fragment implements RecyclerView.OnItemTouchListener{

    private PhotosRecyclerAdapter adapter;
    private RecyclerView recyclerView;
    private GestureDetector gestureDetector;

    public PhotosFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adapter = new PhotosRecyclerAdapter(getContext(), getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES));
        this.gestureDetector = new GestureDetector(getContext(), new MyGestureListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_items, container, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        this.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false));
        this.recyclerView.setAdapter(adapter);
        this.recyclerView.addOnItemTouchListener(this);
//        this.recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, 20, true));
        return view;
    }

    public void updateContent(String path) {
        Log.i(getClass().getName(), "uptadeContent called");
//        this.adapter.notifyItemInserted(this.adapter.getItemCount());
        this.adapter.addPath(path);
    }


    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        this.gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener implements DeleteDialogFragment.OnDeleteActionListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                int position = recyclerView.getChildAdapterPosition(view);
                File file = adapter.getPhotoFileAt(position);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "image/jpg");
                startActivity(intent);
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (view != null) {
                int position = recyclerView.getChildAdapterPosition(view);
                File file = adapter.getPhotoFileAt(position);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DeleteDialogFragment dialog = DeleteDialogFragment.newInstance(file.getAbsolutePath(), position);
                dialog.setmListener(this);
                dialog.show(fm, "DeleteDialogFragment");
                super.onLongPress(e);
            }
            super.onLongPress(e);
        }

        @Override
        public void onDelete(String fileToDelete, int position) {
            adapter.removeFile(position);
        }
    }
//    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
//
//        private int spanCount;
//        private int spacing;
//        private boolean includeEdge;
//
//        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
//            this.spanCount = spanCount;
//            this.spacing = spacing;
//            this.includeEdge = includeEdge;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            int position = parent.getChildAdapterPosition(view); // item position
//            int column = position % spanCount; // item column
//
//            if (includeEdge) {
//                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
//                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
//
//                if (position < spanCount) { // top edge
//                    outRect.top = spacing;
//                }
//                outRect.bottom = spacing; // item bottom
//            } else {
//                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
//                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
//                if (position >= spanCount) {
//                    outRect.top = spacing; // item top
//                }
//            }
//        }
//    }

}
