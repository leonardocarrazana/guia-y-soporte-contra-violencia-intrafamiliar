package bo.edu.ucb.software2;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class DialLaunchDialogFragment extends DialogFragment {

    public static final String HOLDER_KEY = "holder_key";
    public static final String HINT_KEY = "hint_key";

    private OnDialDialogAction listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.listener = (OnDialDialogAction) activity;
        } catch (ClassCastException e) {
            Log.e("Leo", "Activity must implement " + OnDialDialogAction.class.getName());
            throw e;
        }
    }

    public static DialogFragment newDialogFragment(@NonNull TextsReferenceHolder holder, String hint) {
        DialogFragment dialogFragment = new DialLaunchDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(HOLDER_KEY, holder);
        args.putString(HINT_KEY, hint);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.launch_number_edit_layout, null, false);
        final EditText editLaunchNumber = (EditText) view.findViewById(R.id.editLaunchPhoneNumber);
        Bundle args = getArguments();
        editLaunchNumber.setHint(args.getString(HINT_KEY));
        final TextsReferenceHolder holder = (TextsReferenceHolder) args.getSerializable(HOLDER_KEY);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        AlertDialog dialog = builder.setTitle(holder.getDialogTitleRef())
                .setMessage(holder.getDialogMessageRef())
                .setView(view)
                .setCancelable(false)
                .setNegativeButton(R.string.launch_number_dialog_negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onCancel();
                        dialog.cancel();
                    }
                })
                .setPositiveButton(holder.getDialogPositiveButtonRef(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String phoneNumber = editLaunchNumber.getText().toString();
                        listener.onSave(phoneNumber, holder);
                    }
                })
                .create();
        dialog.setCancelable(false);
        return dialog;
    }

    public interface OnDialDialogAction {

        void onSave(String phoneNumber, TextsReferenceHolder dialogType);

        void onCancel();

    }

    public enum TextsReferenceHolder {

        MESSAGE(R.string.launch_number_edit_dialog_title_message, R.string.launch_number_edit_dialog_message_message, R.string.launch_number_edit_dialog_positivebut_message),
        WARNING(R.string.launch_number_edit_dialog_title_warning, R.string.launch_number_edit_dialog_message_warning, R.string.launch_number_edit_dialog_positivebut_warning);

        private int dialogTitleRef;
        private int dialogMessageRef;
        private int dialogPositiveButtonRef;

        TextsReferenceHolder(int dialogTitleRef, int dialogMessageRef, int dialogPositiveButtonRef) {
            this.dialogTitleRef = dialogTitleRef;
            this.dialogMessageRef = dialogMessageRef;
            this.dialogPositiveButtonRef = dialogPositiveButtonRef;
        }

        public int getDialogTitleRef() {
            return dialogTitleRef;
        }

        public int getDialogMessageRef() {
            return dialogMessageRef;
        }

        public int getDialogPositiveButtonRef() {
            return dialogPositiveButtonRef;
        }
    }
}
