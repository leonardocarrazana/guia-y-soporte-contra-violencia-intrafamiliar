package bo.edu.ucb.software2;

import android.media.MediaRecorder;

import java.io.IOException;

/**
 * Created by Leonardo on 02/12/2015.
 */
public class AudioRecorder {

    private static int audioSource = MediaRecorder.AudioSource.MIC;
    private static int outputFormat = MediaRecorder.OutputFormat.MPEG_4;
    private static int audioEncoder = MediaRecorder.AudioEncoder.AAC;

    private MediaRecorder recorder;

    public void prepareForRecord(String newAudioFile) throws IOException {
        this.recorder = new MediaRecorder();
        recorder.setAudioSource(audioSource);
        recorder.setOutputFormat(outputFormat);
        recorder.setOutputFile(newAudioFile);
        recorder.setAudioEncoder(audioEncoder);
        this.recorder.prepare();
    }

    public void startRecording() {
        if (this.recorder == null)
            throw new IllegalStateException("Recorded not prepared for recording");
        this.recorder.start();
    }

    public void stopRecording() {
        if (this.recorder != null) {
            this.recorder.stop();
            this.recorder.release();
            this.recorder = null;
        }
    }

}
