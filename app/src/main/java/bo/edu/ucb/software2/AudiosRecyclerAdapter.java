package bo.edu.ucb.software2;

/**
 * Created by Leonardo on 02/12/2015.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Created by Leonardo on 30/11/2015.
 */
public class AudiosRecyclerAdapter extends RecyclerView.Adapter<AudiosRecyclerAdapter.AudioHolder> {

    private Context context;
    private LayoutInflater inflater;
    private final File audioFilesDirectory;
    private List<String> pathFiles;
    private RecyclerView recyclerView;

    public AudiosRecyclerAdapter(Context context, File imgFilesDirectory) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        if (!imgFilesDirectory.isDirectory())
            throw new IllegalArgumentException("audioFilesDirectory is not a directory.");
        this.audioFilesDirectory = imgFilesDirectory;
        String[] pathFiles = this.audioFilesDirectory.list();
        this.pathFiles = new ArrayList<>(Arrays.asList(pathFiles));
        Collections.sort(this.pathFiles);
    }



    @Override
    public AudioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_row_layout, parent, false);
        AudioHolder audioHolder = new AudioHolder(view);
        audioHolder.getImgIcon().setImageDrawable(context.getResources().getDrawable(R.drawable.ic_music_video_black_48dp));
        audioHolder.getImgIcon().setVisibility(View.VISIBLE);
        return audioHolder;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(AudioHolder holder, final int position) {
//        Log.i(getClass().getName(), "onBindViewHolder w: " + targetWidth + ", h: " + targetHeight);
        holder.txvAudioDesc.setText(this.pathFiles.get(position));
    }

//    @Override
//    public void onViewRecycled(AudioHolder holder) {
//        super.onViewRecycled(holder);
//        holder.getImgIcon().setImageDrawable(context.getResources().getDrawable(R.drawable.ic_image_black_48dp));
//    }

    @Override
    public int getItemCount() {
        return this.pathFiles.size();
    }

    public void addFile(String fileName) {
        this.pathFiles.add(fileName);
        notifyItemInserted(this.pathFiles.size() - 1);
    }

    public File getAudioFileAt(int position) {
        if (position >= 0 && position < this.pathFiles.size()) {
            return new File(this.audioFilesDirectory, pathFiles.get(position));
        } else return null;
    }

    public void removeFile(int position) {
        this.pathFiles.remove(position);
        notifyItemRemoved(position);
    }

    public class AudioHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imgIcon;
        private TextView txvAudioDesc;

        public AudioHolder(View itemView) {
            super(itemView);
            this.imgIcon = (ImageView) itemView.findViewById(R.id.imageIconRow);
            this.txvAudioDesc = (TextView) itemView.findViewById(R.id.txvTitle);
            txvAudioDesc.setOnClickListener(this);
        }

        public ImageView getImgIcon() {
            return imgIcon;
        }

        @Override
        public void onClick(View v) {
            Snackbar.make(v, "Audio selected: " + getAdapterPosition(), Snackbar.LENGTH_SHORT).show();
        }
    }

}
