package bo.edu.ucb.software2;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bo.edu.ucb.software2.ReportsFragment.OnReportActionListener;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DialLaunchDialogFragment.OnDialDialogAction, View.OnClickListener, GuideFragment.ClickListener, OnReportActionListener {

    public static final String ICON_VISIBLE_KEY = "hideIconKey";
    private DrawerLayout drawer = null;
    private SharedPreferences preferences;
    private String currentPhoneNumber;
    private SwitchCompat hideIconToggle;
    private FloatingActionButton fabPhoto;
    private FloatingActionButton fabVideo;
    private FloatingActionButton fabReport;
    private FloatingActionButton fabRecord;
    private FloatingActionButton fabStopRecord;
    private FloatingActionButton fabCurrent;

    private PhotosFragment photosFragment;
    private VideosFragment videosFragment;
    private AudiosFragment audiosFragment;

    private File directoryPathImages;
    private File directoryPathVideos;
    private File directoryPathMusic;

    private String lastImageName;
    private String lastVideoName;
    private String lastAudioName;

    private AudioRecorder recorder;
//    private AudioPlayer player; //creo que aun no implemento esto...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.directoryPathImages = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        this.directoryPathVideos = getExternalFilesDir(Environment.DIRECTORY_MOVIES);
        this.directoryPathMusic = getExternalFilesDir(Environment.DIRECTORY_MUSIC);

        this.recorder = new AudioRecorder();

        fabPhoto = (FloatingActionButton) findViewById(R.id.fabPhoto);
        fabPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Log.i(getClass().getName(), "Storage: " + directoryPathImages.getAbsolutePath());
                lastImageName = "IMG_" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.getDefault()).format(new Date()) + ".jpg";
                Log.i(getClass().getName(), "File: " + lastImageName);
                File file = new File(directoryPathImages, lastImageName);
                Uri uri = Uri.fromFile(file);
                Log.i(getClass().getName(), "Uri: " + uri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, 1);
            }
        });
        fabVideo = (FloatingActionButton) findViewById(R.id.fabVideo);
        fabVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                Log.i(getClass().getName(), "Storage: " + directoryPathVideos.getAbsolutePath());
                lastVideoName = "VID_" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.getDefault()).format(new Date()) + ".mp4";
                Log.i(getClass().getName(), "File: " + lastVideoName);
                File file = new File(directoryPathVideos, lastVideoName);
                Uri uri = Uri.fromFile(file);
                Log.i(getClass().getName(), "Uri: " + uri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                startActivityForResult(intent, 2);
            }
        });

        fabRecord = (FloatingActionButton) findViewById(R.id.fabRecord);
        fabRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastAudioName = "AUD_" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.getDefault()).format(new Date()) + ".mp4";
                Log.i(getClass().getName(), "File: " + lastAudioName);
                File file = new File(directoryPathMusic, lastAudioName);
                try {
                    recorder.prepareForRecord(file.getAbsolutePath());
                    recorder.startRecording();
                    fabRecord.hide();
                    fabCurrent = fabStopRecord;
                    fabStopRecord.show();
//                    Snackbar.make(findViewById(R.id.content_main), "Recording", Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, "Recording", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Snackbar.make(findViewById(R.id.content_main), "Unable to record...", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        fabStopRecord = (FloatingActionButton) findViewById(R.id.fabStopRecord);
        fabStopRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recorder.stopRecording();
                fabStopRecord.hide();
                fabCurrent = fabRecord;
                fabRecord.show();
                if (audiosFragment == null) audiosFragment = (AudiosFragment) getSupportFragmentManager().findFragmentByTag("audioFrag");
                if (audiosFragment != null) audiosFragment.updateContent(lastAudioName);
            }
        });

        fabReport = (FloatingActionButton) findViewById(R.id.fabReport);
        fabReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        MenuItem item = navigationView.getMenu().getItem(0);
        item.setChecked(true);
        onNavigationItemSelected(item);
        this.preferences = getSharedPreferences(DialAppLauncher.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        this.currentPhoneNumber = this.preferences.getString(DialAppLauncher.LAUNCH_NUMBER_KEY, DialAppLauncher.DEFAULT_LAUNCH_NUMBER);
        boolean hideIcon = !preferences.getBoolean(ICON_VISIBLE_KEY, true);
        hideIconToggle = (SwitchCompat) navigationView.getMenu().findItem(R.id.nav_hide_icon).getActionView();
        hideIconToggle.setChecked(hideIcon);
        hideIconToggle.setOnClickListener(this);
        Log.i(getCallingPackage(), "Pruebas: " + getPackageName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id != R.id.nav_records) {
            this.recorder.stopRecording();
        }

        Fragment fragment = null;
        if (id == R.id.nav_guide) {
            fragment = new GuideFragment();
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.content_main, fragment, "guideFrag").commit();
            closeDrawer();
            if (this.fabCurrent != null) this.fabCurrent.hide();
            changeTitle("Guide");
            return true;
        } else if (id == R.id.nav_camera) {
            this.photosFragment = (PhotosFragment) getSupportFragmentManager().findFragmentByTag("photoFrag");
            if (this.photosFragment == null) this.photosFragment = new PhotosFragment();
            fragment = this.photosFragment;
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.content_main, fragment, "photoFrag").commit();
            if (this.fabCurrent != null && this.fabCurrent != this.fabPhoto) this.fabCurrent.hide();
            closeDrawer();
            changeTitle("Photos");
            this.fabPhoto.show();
            this.fabCurrent = this.fabPhoto;
            return true;
        }else
        if (id == R.id.nav_video) {
            this.videosFragment = (VideosFragment) getSupportFragmentManager().findFragmentByTag("videoFrag");
            if (this.videosFragment == null) this.videosFragment = new VideosFragment();
            fragment = this.videosFragment;
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.content_main, fragment, "videoFrag").commit();
            closeDrawer();
            if (this.fabCurrent != null && this.fabCurrent != this.fabVideo) this.fabCurrent.hide();
            this.fabVideo.show();
            this.fabCurrent = this.fabVideo;
            changeTitle("Videos");
            return true;
        } else
        if (id == R.id.nav_records) {
            this.audiosFragment = (AudiosFragment) getSupportFragmentManager().findFragmentByTag("audioFrag");
            if (this.audiosFragment == null) this.audiosFragment = new AudiosFragment();
            fragment = this.audiosFragment;
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.content_main, fragment, "audioFrag").commit();
            if (this.fabCurrent != null && this.fabCurrent != this.fabRecord) this.fabCurrent.hide();
            this.fabRecord.show();
            this.fabCurrent = this.fabRecord;
            closeDrawer();
            changeTitle("Records");
        } else
        if (id == R.id.nav_report) {
            fragment = new ReportsFragment();
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.content_main, fragment, "reportFrag").commit();
            if (this.fabCurrent != null) this.fabCurrent.hide();
            closeDrawer();
            changeTitle("Reports");
//            if (this.fabCurrent != null && this.fabCurrent != this.fabReport) this.fabCurrent.hide();
//            this.fabReport.show();
        } else
//        if (id == R.id.nav_gallery) {
//            fragment = new FragmentTest();
//            if (this.fabCurrent != null && this.fabCurrent != this.fabVideo) this.fabCurrent.hide();
//            this.fabVideo.show();
//            this.fabCurrent = this.fabVideo;
//            closeDrawer();
//        } else if (id == R.id.nav_slideshow) {
//            fragment = new FragmentTest();
//            if (this.fabCurrent != null && this.fabCurrent != this.fabReport) this.fabCurrent.hide();
//            this.fabReport.show();
//            this.fabCurrent = this.fabReport;
//            closeDrawer();
        if (id == R.id.nav_hide_icon) {
//            this.hideIconToggle.performClick();
            return true;
        } else if (id == R.id.nav_launch_number) {
            DialogFragment dialogFragment = DialLaunchDialogFragment.newDialogFragment(DialLaunchDialogFragment.TextsReferenceHolder.MESSAGE, this.currentPhoneNumber);
            FragmentManager fragmentManager = getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "Launch Dial Number Edit Dialog Message");
            closeDrawer();
            return true;
        }
//        FragmentManager fm = getSupportFragmentManager();
//        fm.beginTransaction().replace(R.id.content_main, fragment).commit();
        return true;
    }

    private void changeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void closeDrawer() {
        if (this.drawer == null)
            this.drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void setIconVisible(boolean visible) {
        if (this.preferences.getBoolean(ICON_VISIBLE_KEY, true) != visible) {
            PackageManager packageManager = getPackageManager();
            ComponentName componentName = new ComponentName(this, "bo.edu.ucb.software2.Launcher");
            int componentEnabledState = visible ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
            packageManager.setComponentEnabledSetting(componentName, componentEnabledState, PackageManager.DONT_KILL_APP);
            this.preferences.edit().putBoolean(ICON_VISIBLE_KEY, visible).apply();
            Log.i(getClass().getName(), "setIconVisible(" + visible + ")" + this.preferences.getBoolean(ICON_VISIBLE_KEY, true) + " " + visible);
        }
    }

    @Override
    public void onSave(String phoneNumber, DialLaunchDialogFragment.TextsReferenceHolder dialogType) {
        Log.i(getClass().getName(), "onSave");
        if (!phoneNumber.isEmpty()) {
            Log.i(getClass().getName(), "if not empty number: " + phoneNumber);
            this.currentPhoneNumber = phoneNumber;
            this.preferences.edit().putString(DialAppLauncher.LAUNCH_NUMBER_KEY, phoneNumber).apply();
            Log.i(getClass().getName(), "saved number?: " + this.preferences.getString(DialAppLauncher.LAUNCH_NUMBER_KEY, "Default"));
        }
        if (this.hideIconToggle.isChecked() && dialogType == DialLaunchDialogFragment.TextsReferenceHolder.WARNING) {
            setIconVisible(false);
            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            // Image captured and saved to fileUri specified in the Intent
            if (resultCode == RESULT_OK) {
//                Toast.makeText(this, "Image saved to:\n" +
//                        data.getData(), Toast.LENGTH_LONG).show();
                if (this.photosFragment == null) this.photosFragment = (PhotosFragment) getSupportFragmentManager().findFragmentByTag("photoFrag");
                if (this.photosFragment != null) this.photosFragment.updateContent(this.lastImageName);
                Log.i(getClass().getName(), "onActivityResult request code 1 and ok: " + data.getData());
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
        if (requestCode == 2) {
            // Image captured and saved to fileUri specified in the Intent
            if (resultCode == RESULT_OK) {
//                Toast.makeText(this, "Image saved to:\n" +
//                        data.getData(), Toast.LENGTH_LONG).show();
                if (this.videosFragment == null) this.videosFragment = (VideosFragment) getSupportFragmentManager().findFragmentByTag("videoFrag");
                if (this.videosFragment != null) this.videosFragment.updateContent(this.lastVideoName);
                Log.i(getClass().getName(), "onActivityResult request code 1 and ok: " + data.getData());
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
        if (requestCode == 1234) {
            if (resultCode == RESULT_OK) {
                Snackbar.make(findViewById(R.id.content_main), "E-mail sent", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onCancel() {
        Log.i(getClass().getName(), "onCancel");
        boolean iconVisible = this.preferences.getBoolean(ICON_VISIBLE_KEY, true);
        if(this.hideIconToggle.isChecked() == iconVisible) {
            Log.i(getClass().getName(), "if " + this.hideIconToggle.isChecked() + " " + iconVisible);
            this.hideIconToggle.setChecked(false);
        }
    }

    @Override
    public void onClick(View v) {
        Log.i(getClass().getName(), "onClick");
        if (this.hideIconToggle.isChecked()) {
            Log.i(getClass().getName(), "if - true");
            DialogFragment dialogFragment = DialLaunchDialogFragment.newDialogFragment(DialLaunchDialogFragment.TextsReferenceHolder.WARNING, this.currentPhoneNumber);
            FragmentManager fragmentManager = getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "Launch Dial Number Edit Dialog Warning");
        } else setIconVisible(true);
    }

    @Override
    public void onClick(int position) {
        String[] titles = getResources().getStringArray(R.array.titles);
        String[] messages = getResources().getStringArray(R.array.guides);
        DialogFragment dialogFragment = GuideDialogFragment.newInstance(titles[position], messages[position]);
        dialogFragment.show(getSupportFragmentManager(), "GuideMessageDialog");
    }

    @Override
    public void onSendReport(ReportsFragment.Report report) {
        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setData(Uri.parse("mailto:" + report.getEmail())); // or just "mailto:" for blank
        intent.setType("message/rfc822"); //Email message MIME type
//        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{report.getEmail()});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Reporte de violencia intrafamiliar.");
        intent.putExtra(Intent.EXTRA_TEXT, report.getFullName() + "\n" + "Telefono: " + report.getPhoneNumber() + "\n" + report.getDescription());
        PackageManager packageManager = getPackageManager();
        List activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe) {
            startActivityForResult(intent, 1234);
        } else {
            Snackbar.make(findViewById(R.id.content_main), "There is no app able to handle this action.", Snackbar.LENGTH_LONG).show();
        }
//        try {
////            startActivity(intent);
//            startActivityForResult(Intent.createChooser(intent, "Send mail..."), 1234);
//        } catch (android.content.ActivityNotFoundException ex) {
//            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//        }
    }

}
