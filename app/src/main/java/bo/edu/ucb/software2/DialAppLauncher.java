package bo.edu.ucb.software2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DialAppLauncher extends BroadcastReceiver {

    public static final String SHARED_PREFERENCES_NAME = "bo.edu.ucb.software2.shared_preferences";
    public static final String LAUNCH_NUMBER_KEY = "launch_number_key";
    public static final String DEFAULT_LAUNCH_NUMBER = "123";

    public DialAppLauncher() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Leo BR", "onReceive");
        String launch_number = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).getString(LAUNCH_NUMBER_KEY, DEFAULT_LAUNCH_NUMBER);
//        Log.i("Leo BR", "is this my preferences? " + );
        String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        Log.i("Leo BR", "Phone dialed: " + phoneNumber);
        if (launch_number.equals(phoneNumber)) {
            Log.i("Leo BR", "inside if");
            setResultData(null);
            Intent appIntent = new Intent(context, MainActivity.class);
            appIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(appIntent);
            Log.i("Leo BR", "Activity should be launched");
        }

    }
}
